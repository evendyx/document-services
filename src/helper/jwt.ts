import jwt from 'jsonwebtoken'


function verifyToken(token){
    // console.log(token, process.env.SECRET_KEY)
    return jwt.verify(token, process.env.SECRET_KEY)
}

export default verifyToken