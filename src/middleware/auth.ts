import verifyToken from '../helper/jwt'

function auth(req, res, next){
    const { authorization } = req.headers
    // console.log(req.headers)
    try {
        const decoded = verifyToken(authorization.split(' ')[1])
        if(decoded){
            const { user_id, company_id } = decoded
            req.user_id = user_id
            req.company_id = company_id
            next()
        } else {
            res.status(401).json({
                message: 'not authorized'
            })
        }
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message: error.message
        })
    }
}

export default auth