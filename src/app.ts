import express from 'express'
import cors from 'cors'
import mongoose from 'mongoose'
import router from'./router'
import dotenv from 'dotenv'

dotenv.config()

const app = express()
const port = process.env.PORT || 3000

mongoose.connect('mongodb+srv://admin:admin@cluster0.srfnv.mongodb.net/test_db?retryWrites=true&w=majority', {useNewUrlParser: true, useUnifiedTopology: true})

app.use(cors())
app.use(express.urlencoded({extended: true}))
app.use(express.json())
app.use(router)

app.listen(port, () => {
    // console.log(process.env)
    console.log(`listen to port: ${port}`)
})