import Document from '../model/documentModel'
import Folder from '../model/folderModel'
import redis from '../redis'

class DocumentController{
    static async getAll(req, res){
        try {
            const cache = await redis.get('services')
            if (cache) {
              res.status(200).json({
                error: false,
                data: JSON.parse(cache),
              });
            } else {
              let data: object[] = [];
              const documents = await Document.find();
              const folders = await Folder.find({
                $or: [
                  { is_public: true },
                  { is_public: false, owner_id: req.user_id },
                ],
              }); //If is_public is true, all user from a company can see the file or folder. If is_public is false, only owner can see the documents.
              data = documents.concat(folders);
              await redis.set("services", JSON.stringify(data)); // set cache
              res.status(200).json({
                error: false,
                data,
              });
            }
        } catch (error) {
            res.status(500).json({error})
        }
    }

    static async getListFile(req, res){
        const { folder_id } = req.params
        try {
            const cache = await redis.get('folder')
            if(cache){
                let data = JSON.parse(cache)
                const isSame = (value) => value === folder_id
                const datas = data.map(elm => {
                               return elm.folder_id
                })
                if(datas.every(isSame)){
                    res.status(200).json({
                        error: false,
                        data
                    })
                } else {
                    data = await Document.find({folder_id})
                    await redis.set('folder', JSON.stringify(data))
                    res.status(200).json({
                    error: false,
                    data
                })
                }
            } else {
                const data = await Document.find({folder_id})
                await redis.set('folder', JSON.stringify(data))
                res.status(200).json({
                    error: false,
                    data
                })
            }
        } catch (error) {
            res.status(500).json({
                error
            })
        }
    }

    static async getDetailDocument(req, res){
        const { document_id } = req.params
        try {
            const cache = await redis.get('document')
            if(cache){
                let data = JSON.parse(cache)
                if(data.id === document_id){
                    res.status(200).json({
                        error: false,
                        data
                    })
                } else {
                    data = await Document.findOne({id: document_id})
                    await redis.set('document', JSON.stringify(data))
                    res.status(200).json({
                        error: false,
                        data
                    })
                }
            } else {
                const data = await Document.findOne({id: document_id})
                await redis.set('document', JSON.stringify(data))
                res.status(200).json({
                    error: false,
                    data
                })
            }
        } catch (error) {
            res.status(500).json({
                error
            })
        }
    }

    static async setFolder(req, res){
      console.log(req.body);
      const { id, name, timestamp, is_public, owner_id } = req.body;
      const user_id = req.user_id;
      const company_id = req.company_id;
      const newOwnerid =
        owner_id === "" || !owner_id
          ? req.user_id.toString()
          : owner_id.toString();
      //user yg lagi login. atau user yang mau diserah terimakan dokumennya. Kalau 0, backend ambil dari data login
      try {
        const checker = await Folder.findOne({ id });

        console.log(checker);
        if (checker) {
          const folder = {
            id,
            name,
            timestamp,
          };
          const data = await Folder.updateOne(folder);
          res.status(201).json({
            error: false,
            message: "folder updated",
            data,
          });
        } else {
          const folder = new Folder({
            id,
            name,
            timestamp,
            is_public,
            owner_id: newOwnerid,
            company_id,
          });
          console.log(folder);
          const cache = await redis.get("services");
          const cacheData = JSON.parse(cache);
          const data = await folder.save();
          cacheData.push(data);
          await redis.set("services", JSON.stringify(cacheData));
          res.status(201).json({
            error: false,
            message: "folder created",
            data,
          });
        }
      } catch (error) {
        console.log(error);
        res.status(500).json({
          error,
        });
      }
    }

    static async setDocument(req, res){
      const {
        id,
        name,
        type,
        folder_id,
        content,
        timestamp,
        share,
        company_id,
        owner_id,
      } = req.body;
      const newOwnerid =
        owner_id === "" || !owner_id
          ? req.user_id.toString()
          : owner_id.toString();
      //user yg lagi login. atau user yang mau diserah terimakan dokumennya. Kalau 0, backend ambil dari data login
      // console.log(newOwnerid.toString())
      try {
        const newDocument = new Document({
          id,
          name,
          type,
          folder_id,
          content,
          timestamp,
          owner_id: newOwnerid,
          share,
          company_id,
        });
        console.log(newDocument);
        const cache = await redis.get("services");
        const cacheData = JSON.parse(cache);
        const document = await newDocument.save();
        cacheData.push(document);
        await redis.set("services", JSON.stringify(cacheData));
        res.status(201).json({
          error: false,
          message: "success set document",
          data: document,
        });
      } catch (error) {
        res.status(500).json({
          error,
        });
      }
    }

    static async deleteFolder(req, res){
        const { id } = req.body
        try {
            const cache = await redis.get('services')
            const cacheData = JSON.parse(cache)
            for (let i = 0; i < cacheData.length; i++) {
                if(cacheData[i].id === id){
                    cacheData.splice(i, 1)
                }
            }
            await redis.set('services', JSON.stringify(cacheData))
            const data = await Folder.findOneAndDelete({id})
            res.status(200).json({
                error: false,
                message: 'Success delete folder'
            })
        } catch (error) {
            res.status(500).json({
                error: 'internal server error'
            })
        }
    }

    static async deleteDocument(req, res){
        const { id } = req.body
        try {
            const cache = await redis.get('services')
            const cacheData = JSON.parse(cache)
            for (let i = 0; i < cacheData.length; i++) {
                if(cacheData[i].id === id){
                    cacheData.splice(i, 1)
                }
            }
            await redis.set('services', JSON.stringify(cacheData))
            const data = await Document.findOneAndDelete({id})
            console.log(data)
            res.status(200).json({
                error: false,
                message: 'Success delete document'
            })
        } catch (error) {
            res.status(500).json({
                error: 'internal server error'
            })
        }
    }
}

export default DocumentController